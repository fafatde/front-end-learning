module.exports = {
    // singleQuote: true,
    // trailingComma: 'all',

    tabWidth: 4,
    trailingComma: 'es5',
    semi: true,
    singleQuote: true,
};