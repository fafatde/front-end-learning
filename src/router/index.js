import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from '@/views/Index.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Index',
        component: Index,
    },
    {
        path: '/homework/Conversion0708',
        name: 'Conversion0708',
        component: () =>
            import(
                /* webpackChunkName: "homework" */ '@/views/homework/Conversion0708.vue'
            ),
    },
    {
        path: '/homework/MonacoEditor0710',
        name: 'MonacoEditor0710',
        component: () =>
            import(
                /* webpackChunkName: "homework" */ '@/views/homework/MonacoEditor0710.vue'
            ),
    },
    {
        path: '/week02/FindKthLargestElement',
        name: 'FindKthLargestElement',
        component: () =>
            import(
                /* webpackChunkName: "week02" */ '@/views/week02/FindKthLargestElement.vue'
            ),
    },
    {
        path: '/week02/RenderTemplate',
        name: 'RenderTemplate',
        component: () =>
            import(
                /* webpackChunkName: "week02" */ '@/views/week02/RenderTemplate.vue'
            ),
    },
    {
        path: '/week03/OnlinePreview',
        name: 'OnlinePreview',
        component: () =>
            import(
                /* webpackChunkName: "week02" */ '@/views/week03/OnlinePreview.vue'
            ),
    },
];

const router = new VueRouter({
    routes,
});

export default router;
