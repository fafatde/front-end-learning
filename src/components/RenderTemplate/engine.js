/**
 * 在老师的基础上改的v-if [捂脸]🤦‍
 *
 * 老师的demo的两个bug：
 * 1、图片在右边
 *    parseNodeToDom(..) 里的使用栈stack.pop() 应改成使用队列queue.shift()，
 * 2、for news-item 在newslist之外
 *    pdom.parentNode.appendChild(ele); 应改成  pdom.appendChild(ele);
 */

import Vnode from './vnode.js';

export default class Engine {
    constructor() {
        this.nodes = new Map();
    }

    render(template, data) {
        let tmplUUID = this.createNodeAndReplaceTemplate(template);

        console.log('第一阶段|解析创建node>>>', this.nodes);

        let rootNode = this.parseToNodeTree(tmplUUID);

        console.log('第二阶段|构建nodeTree>>>', rootNode);

        let dom = this.parseNodeToDom(rootNode, data);

        console.log('第三阶段|nodeTree To DomTree>>>', dom);

        return dom;
    }

    createNodeAndReplaceTemplate(template) {
        const re1 = /<(\w+)\s*([^>]*)>([^<]*)<\/\1>/gm;
        const re2 = /<(\w+)\s*([^(/>)]*)\/>/gm;

        template = template.replace(/\n/gm, '');
        while (re1.test(template) || re2.test(template)) {
            template = template.replace(re1, (s0, s1, s2, s3) => {
                let attr = this.parseAttributeToMap(s2);
                console.log(attr);
                let node = new Vnode(s1, attr, [], null, s3); // (tag, attr, children, parent, childrenTemplate)
                this.nodes.set(node.uuid, node);

                return `(${node.uuid})`;
            });

            template = template.replace(re2, (s0, s1, s2) => {
                let attr = this.parseAttributeToMap(s2);
                let node = new Vnode(s1, attr, [], null, '');
                this.nodes.set(node.uuid, node);

                return `(${node.uuid})`;
            });
        }
        return template;
    }

    parseToNodeTree(template) {
        template = template.trim();
        let stack = [];
        let re = /\((.*?)\)/g;
        let parent = new Vnode('root', {}, [], null, template);
        stack.push(parent);

        while (stack.length > 0) {
            let pnode = stack.pop();
            let nodeStr = pnode.childrenTemplate.trim();

            [...nodeStr.matchAll(re)].forEach((item) => {
                let n = this.nodes.get(item[1]); //item[1] 匹配到第一个()里的值  uuid bb3c07b7-2a91-480e-99ad-73de32321661
                let newn = new Vnode(
                    n.tag,
                    n.attr,
                    [],
                    pnode,
                    n.childrenTemplate
                );
                pnode.children.push(newn);
                stack.push(newn);
            });
        }
        return parent.children[0];
    }

    parseNodeToDom(rootNode, data) {
        let fragment = document.createDocumentFragment();
        let queue = [[rootNode, fragment, data]];

        while (queue.length > 0) {
            let [pnode, pdom, data] = queue.shift(); // 先进先出

            if (pnode.attr.get('v-if')) {
                //取v-if里的值
                let props = pnode.attr.get('v-if').split('.');
                let condition = data[props[0]];
                props.slice(1).forEach((item) => {
                    condition = condition[item];
                });

                if (condition) {
                    let html = this.parseHTML(pnode, data); // 替换掉{{}}里的值
                    let ele = this.createElement(pnode, html); // 创建节点，及其属性值，属性值可能包括动态数据的值
                    this.parseAttributeData(ele, pnode, data); // 处理属性的{{}}
                    pdom.appendChild(ele);

                    pnode.children.forEach((item) => {
                        queue.push([item, ele, data]);
                    });
                }
            } else {
                let html = this.parseHTML(pnode, data);
                let ele = this.createElement(pnode, html);
                this.parseAttributeData(ele, pnode, data);
                pdom.appendChild(ele);

                pnode.children.forEach((item) => {
                    queue.push([item, ele, data]);
                });
            }
        }

        return fragment;
    }

    // 将class="xxx" v-if="xxx" 转为 map
    parseAttributeToMap(str) {
        const attrs = new Map();
        str.replace(/(\w+-?\w+)\s*=["'](.*?)["']/gm, (s0, s1, s2) => {
            attrs.set(s1, s2);
        });
        return attrs;
    }

    // 替换掉{{}}里的值
    parseHTML(node, data) {
        return node.childrenTemplate.replace(/\{\{(.*?)\}\}/g, (s0, s1) => {
            let props = s1.split('.');
            let val = data[props[0]];
            props.slice(1).forEach((item) => {
                val = val[item];
            });
            return val;
        });
    }

    createElement(node, html) {
        let ignoreAttr = ['click', 'v-if'];
        let dom = document.createElement(node.tag);

        for (let [key, value] of node.attr) {
            if (!ignoreAttr.includes(key)) {
                dom.setAttribute(key, value);
            }
        }

        if (node.children.length === 0) {
            dom.innerHTML = html;
        }

        return dom;
    }

    // 处理有{{}}的属性
    parseAttributeData(ele, node, data) {
        for (let [key, value] of node.attr) {
            let result = /\{\{(.*?)\}\}/.exec(value);
            if (result && result.length > 0) {
                let props = result[1].split('.');
                let val = data[props[0]];
                props.slice(1).forEach((item) => {
                    val = val[item];
                });

                ele.setAttribute(key, val);
            }
        }
    }
}
