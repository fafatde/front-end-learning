import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// import { Container, Header, Aside, Main, Button } from 'element-ui';

// Vue.use(Container);
// Vue.use(Header);
// Vue.use(Button);
// Vue.use(Main);
// Vue.use(Aside);

Vue.config.productionTip = false;

Vue.use(ElementUI, {
    size: 'middle',
});

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
